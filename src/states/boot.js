import phaser from 'phaser';
import preloadBar from '../../assets/images/preloader-bar.png';

export default class Boot extends phaser.State {
  init() {
    this.game.stage.backgroundColor = '#fff';
    this.scale.scaleMode = phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignVertically = true;
    this.scale.pageAlignHorizontally = true;
    this.game.physics.startSystem(phaser.Physics.ARCADE);
  }
  preload() {
    this.load.image('preloadBar', preloadBar);
  }
  create() {
    this.state.start('Preload');
  }
}
