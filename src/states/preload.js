import phaser from 'phaser';
import background from '../../assets/images/background.png';
import blood from '../../assets/images/blood.png';
import bullet from '../../assets/images/bullet.png';
import button from '../../assets/images/button.png';
import buttonChili from '../../assets/images/button_chilli.png';
import buttonPlant from '../../assets/images/button_plant.png';
import buttonSunflower from '../../assets/images/button_sunflower.png';
import chickenSheet from '../../assets/images/chicken_sheet.png';
import chili from '../../assets/images/chilli.png';
import deadZombie from '../../assets/images/dead_zombie.png';
import plantSheet from '../../assets/images/plant_sheet.png';
import sunSheet from '../../assets/images/sun_sheet.png';
import sunflower from '../../assets/images/sunflower.png';
import zombieSheet from '../../assets/images/zombie_sheet.png';
import hitSoundMP3 from '../../assets/audio/hit.mp3';
import hitSoundOgg from '../../assets/audio/hit.ogg';

export default class Preload extends phaser.State {
  preload() {
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadBar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(3);

    this.load.setPreloadSprite(this.preloadBar);

    this.load.image('background', background);
    this.load.image('blood', blood);
    this.load.image('bullet', bullet);
    this.load.image('button', button);
    this.load.image('buttonChili', buttonChili);
    this.load.image('buttonPlant', buttonPlant);
    this.load.image('buttonSunflower', buttonSunflower);
    this.load.image('sunflower', sunflower);
    this.load.image('chili', chili);
    this.load.image('deadZombie', deadZombie);

    this.load.spritesheet('chickenSheet', chickenSheet, 25, 25, 3, 1, 2);
    this.load.spritesheet('plantSheet', plantSheet, 24, 40, 3, 1, 2);
    this.load.spritesheet('sunSheet', sunSheet, 30, 30, 2, 1, 2);
    this.load.spritesheet('zombieSheet', zombieSheet, 30, 50, 3, 1, 2);

    this.load.audio('hit', [hitSoundMP3, hitSoundOgg]);
    // This is redundant, webpack is already loading ours
    // json files so we do not have to load them again.
    // Whenever we need to use the json, we just import it
    // this.load.text('buttonsText', buttonsText);
    // this.load.text('level1', level1);
    // this.load.text('level2', level2);
  }
  create() {
    this.state.start('Game');
  }
}
