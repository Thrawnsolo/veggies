import phaser from 'phaser';
import buttonsText from '../../assets/data/buttons.json';
import level1 from '../../assets/data/level1.json';
import level2 from '../../assets/data/level2.json';
import Plant from '../prefabs/plant';
import Zombie from '../prefabs/zombie';
import Sun from '../prefabs/sun';

const attackPlant = (plant, zombie) => {
  plant.damage(zombie.attack);
};

const hitZombie = function (bullet, zombie) {
  bullet.kill();
  zombie.damage(1);
  this.hitSound.play();
  if (!zombie.alive) {
    this.killedEnemies += 1;
    // Next level when they are all dead
    if (this.killedEnemies === this.numEnemies) {
      this.game.state.start('Game', true, false, this.levelData.nextLevel);
    }
  }
};

const levelMap = { level1, level2 };
export default class Game extends phaser.State {
  init(currentLevel) {
    this.currentLevel = levelMap[currentLevel] || level1;
    this.game.physics.arcade.gravity.y = 0;
    this.SUN_FREQUENCY = 5;
    this.SUN_VELOCITY = 50;
    this.ZOMBIE_Y_POSITIONS = [49, 99, 149, 199, 249];
    this.ZOMBIE_X = 0;
  }
  scheduleNextEnemy() {
    const nextEnemy = this.levelData.zombies[this.currentEnemyIndex];
    if (nextEnemy) {
      const nextTime = 1000 * (nextEnemy.time + (this.currentEnemyIndex === 0 ? 0 :
        this.levelData.zombies[this.currentEnemyIndex - 1].time
      ));
      this.nextEnemyTime = this.game.time.events.add(nextTime, () => {
        const zombiePosition = Math.floor(Math.random() * this.ZOMBIE_Y_POSITIONS.length);
        const y = this.ZOMBIE_Y_POSITIONS[zombiePosition];
        this.createZombie(this.game.world.width + 40, y, nextEnemy);
        this.currentEnemyIndex += 1;
        this.scheduleNextEnemy();
      });
    }
  }
  loadLevel() {
    this.levelData = this.currentLevel;
    // Keeps track of the next enemy to be shown
    this.currentEnemyIndex = 0;

    this.killedEnemies = 0;
    this.numEnemies = this.levelData.zombies.length;
    this.scheduleNextEnemy();
  }
  create() {
    this.hitSound = this.add.audio('hit');
    this.loadLevel();
    this.background = this.add.sprite(0, 0, 'background');
    this.createLandPatches();
    this.bullets = this.add.group();
    this.plants = this.add.group();
    this.zombies = this.add.group();
    this.suns = this.add.group();

    // Player stats
    this.numSuns = 100;

    // Lets create the GUI
    this.createGUI();

    // create new suns with a certain frequency
    this.sunGenerationTimer = this.game.time.create(false);
    this.sunGenerationTimer.start();
    this.scheduleSunGeneration();
  }
  generateRandomSun() {
    // Position of the sun
    const y = -20;
    const x = 40 + (420 * Math.random());
    const sun = this.createSun(x, y);
    sun.body.velocity.y = this.SUN_VELOCITY;
  }
  scheduleSunGeneration() {
    this.sunGenerationTimer.add(phaser.Timer.SECOND * this.SUN_FREQUENCY, () => {
      this.generateRandomSun();
      this.scheduleSunGeneration();
    }, this);
  }
  createPlant(x, y, data, patch) {
    let newPlant = this.plants.getFirstDead();
    if (!newPlant) {
      newPlant = new Plant(this, x, y, data, patch);
      this.plants.add(newPlant);
    } else {
      newPlant.reset(x, y, data, patch);
    }
    return newPlant;
  }
  createZombie(x, y, data) {
    let newZombie = this.zombies.getFirstDead();
    if (!newZombie) {
      newZombie = new Zombie(this, x, y, data);
      this.zombies.add(newZombie);
    } else {
      newZombie.reset(x, y, data);
    }
    return newZombie;
  }
  createSun(x, y) {
    let newSun = this.suns.getFirstDead();
    if (!newSun) {
      newSun = new Sun(this, x, y);
      this.suns.add(newSun);
    } else {
      newSun.reset(x, y);
    }
    return newSun;
  }
  update() {
    this.game.physics.arcade.collide(this.plants, this.zombies, attackPlant, null, this);
    this.game.physics.arcade.collide(this.bullets, this.zombies, hitZombie, null, this);
    // You can set the update of the elements in the main game or in each of the elements.
    // I am leaving this one here as an example as what can be achieved, but use the one in
    // Zombie class
    /* this.zombies.forEachAlive((zombie) => {
      const aliveZombie = zombie;
      // zombies need to keep their speed
      aliveZombie.body.velocity.x = zombie.defaultVelocity;
      // Game over if any zombie reach the house
      if (aliveZombie.x <= this.HOUSE_X) {
        this.gameOver();
      }
    }); */
  }
  gameOver() {
    this.game.state.start('Game');
  }
  clickButton(clickedButton) {
    const button = clickedButton;
    if (!button.selected) {
      this.clearSelection();
      this.plantLabel.text = `Cost: ${button.plantData.cost}`;
      // Check if we have enough money for it
      if (this.numSuns >= button.plantData.cost) {
        button.selected = true;
        button.alpha = 0.5;
        // Keep a track of the selected plant
        this.currentSelection = button.plantData;
      } else {
        this.plantLabel.text += ' - Too expensive';
      }
    } else {
      this.clearSelection();
    }
  }
  clearSelection() {
    this.currentSelection = null;
    this.plantLabel.text = '';
    this.buttons.forEach((buttonElement) => {
      const button = buttonElement;
      button.alpha = 1;
      button.selected = false;
    });
  }
  createLandPatches() {
    this.landPatches = this.add.group();
    const rectangle = this.add.bitmapData(40, 50);
    rectangle.ctx.fillStyle = 'black';
    rectangle.ctx.fillRect(0, 0, 40, 50);
    let dark = false;
    for (let i = 0; i < 10; i += 1) {
      for (let j = 0; j < 5; j += 1) {
        const patch = new phaser.Sprite(this.game, 64 + (i * 40), 24 + (j * 50), rectangle);
        patch.alpha = dark ? 0.2 : 0.1;
        dark = !dark;
        patch.isBusy = false;
        this.landPatches.add(patch);
        // Plant something if the patch is available and a plant is selected
        patch.inputEnabled = true;
        patch.events.onInputDown.add(this.plantPlant, this);
      }
    }
  }
  plantPlant(patchObject) {
    if (!patchObject.isBusy &&
      this.currentSelection &&
      this.currentSelection.cost <= this.numSuns) {
      const patch = patchObject;
      patch.isBusy = true;
      // Create a new Plant
      this.createPlant(
        patch.x + (patch.width / 2),
        patch.y + (patch.height / 2),
        this.currentSelection,
        patch
      );
      // Substract the cost
      this.increaseSun(-this.currentSelection.cost);
      this.clearSelection();
    }
  }
  createGUI() {
    // Show the sun stats
    const sun = this.add.sprite(20, this.game.height - 20, 'sunSheet');
    sun.anchor.setTo(0.5);
    sun.scale.setTo(0.75);
    const style = { font: '68 Arial', fill: 'red' };
    this.sunLabel = this.add.text(30, this.game.height - 25, '', style);
    this.updateStats();

    // Lets create th button bar

    this.buttonData = buttonsText;

    this.buttons = this.add.group();
    this.buttonData.forEach((element, index) => {
      const button = new phaser.Button(
        this.game,
        80 + (index * 40),
        this.game.height - 35,
        element.btnAsset,
        this.clickButton,
        this
      );
      this.buttons.add(button);
      // Pass the data to the button
      button.plantData = element;
    });
    // Lint does not approves the use of for of, but I wanted to write it just as an exercise
    // Do whatever the fuck you want as long as you keep the code readable.
    /* for (const [index, value] of this.buttonData.entries()) {
      const button = new phaser.Button(
        this.game,
        80 + (index * 40),
        this.game.height - 35,
        value.btnAsset, this.clickButton,
        this
      );
      this.buttons.add(button);
      button.plantData = value;
    } */

    this.plantLabel = this.add.text(270, this.game.height - 20, '', style);
  }
  updateStats() {
    this.sunLabel.text = this.numSuns;
  }
  increaseSun(amount) {
    this.numSuns += amount;
    this.updateStats();
  }
}
