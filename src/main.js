import pixi from 'pixi';
import p2 from 'p2';
import phaser from 'phaser';
import Boot from './states/boot';
import Game from './states/game';
import Preload from './states/preload';

class Main extends phaser.Game {
  static getDimensions(maxWidth, maxHeight) {
    const width = window.innerWidth * window.devicePixelRatio;
    const height = window.innerHeight * window.devicePixelRatio;
    let landscapeWidth = Math.max(width, height);
    let landscapeHeight = Math.min(width, height);

    if (landscapeWidth > maxWidth) {
      const ratioWidth = maxWidth / landscapeWidth;
      landscapeWidth *= ratioWidth;
      landscapeHeight *= ratioWidth;
    }
    if (landscapeHeight > maxHeight) {
      const ratioHeight = maxHeight / landscapeHeight;
      landscapeWidth *= ratioHeight;
      landscapeHeight *= ratioHeight;
    }
    return { width: landscapeWidth, height: landscapeHeight };
  }

  constructor() {
    const dimensions = Main.getDimensions(480, 320);
    super(dimensions.width, dimensions.height, phaser.AUTO);
    this.state.add('Boot', Boot);
    this.state.add('Preload', Preload);
    this.state.add('Game', Game);
  }
}

window.game = new Main();
window.game.state.start('Boot');
