import phaser from 'phaser';

// This is me playing around and trying to recreate private functions
// Arrow function does not work in this scenario
function addAndPlayAnimation(data) {
  if (data.animationFrames) {
    this.animationName = `${data.asset}Anim`;
    this.animations.add(this.animationName, data.animationFrames, 4, true);
    this.play(this.animationName);
  }
}
const HOUSE_X = 60;
export default class Zombie extends phaser.Sprite {
  constructor(state, x, y, data) {
    super(state.game, x, y, data.asset);
    this.state = state;
    this.game = state.game;
    this.anchor.setTo(0.5);
    this.game.physics.arcade.enable(this);
    this.reset(x, y, data);
  }
  reset(x, y, data) {
    super.reset(x, y, data.health);
    this.loadTexture(data.asset);
    this.animationName = null;
    // For private functions to work, you have to bind this.
    addAndPlayAnimation.bind(this)(data);
    this.attack = data.attack;
    this.defaultVelocity = data.velocity;
    this.body.velocity.x = data.velocity;
  }
  update() {
    // zombies need to keep their speed
    this.body.velocity.x = this.defaultVelocity;
    // Game over if any zombie reach the house
    if (this.x <= HOUSE_X) {
      this.state.gameOver();
    }
  }
  damage(amount) {
    super.damage(amount);
    // Particle effect
    const emitter = this.game.add.emitter(this.x, this.y, 50);
    emitter.makeParticles('blood');
    emitter.minParticleSpeed.setTo(-100, -100);
    emitter.maxParticleSpeed.setTo(100, 100);
    emitter.gravity = 300;
    emitter.start(true, 200, null, 100);
    if (this.health <= 0) {
      const corpse = this.game.add.sprite(this.x, this.bottom, 'deadZombie');
      corpse.anchor.setTo(0.5, 1);
    }
  }
}
