import phaser from 'phaser';

export default class Sun extends phaser.Sprite {
  constructor(state, x, y) {
    super(state.game, x, y, 'sunSheet');
    this.state = state;
    this.game = state.game;
    this.game.physics.arcade.enable(this);

    this.animations.add('shine', [0, 1], 10, true).play();
    this.anchor.setTo(0.5);
    // Enable input in order to make it interactive
    this.inputEnabled = true;
    this.input.pixelPerfectClick = true;
    this.events.onInputDown.add(this.collectSun, this);
    // Expiration for the sun
    this.sunExpirationTimer = this.game.time.create(false);
    this.reset(x, y);
  }
  scheduleExpiration() {
    this.sunExpirationTimer.start();
    // Random expiration time between 1 and 4 seconds
    const expirationTime = (1 + (Math.random() * 3)) * 1000;
    this.sunExpirationTimer.add(phaser.Timer.SECOND + expirationTime, this.kill, this);
  }
  kill() {
    // The kill method set the object to dead but it is still there, not like the destroy object,
    // which marks it to be collected by the garbage collector.
    this.sunExpirationTimer.stop();
    super.kill();
  }
  reset(x, y) {
    super.reset(x, y);
    this.scheduleExpiration();
  }
  collectSun() {
    this.state.increaseSun(25);
    this.kill();
  }
}
