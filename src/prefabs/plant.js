import phaser from 'phaser';
import Bullet from './bullet';
// This is me playing around and trying to recreate private functions
// Arrow function does not work in this scenario
function addAnimation(data) {
  if (data.animationFrames) {
    this.animationName = `${data.plantAsset}Anim`;
    this.animations.add(this.animationName, data.animationFrames, 6, false);
  }
}
export default class Plant extends phaser.Sprite {
  constructor(state, x, y, data, patch) {
    super(state.game, x, y, data.plantAsset);
    this.state = state;
    this.game = state.game;
    this.bullets = state.bullets;
    this.suns = state.suns;
    this.anchor.setTo(0.5);
    this.game.physics.arcade.enable(this);
    this.body.immovable = true;

    // We need a timer for the sun and the shooting
    // Make sure that they are stopped when the plan has been killed
    this.shootingTimer = this.game.time.create(false);
    this.producingTimer = this.game.time.create(false);

    this.reset(x, y, data, patch);
  }
  reset(x, y, data, patch) {
    super.reset(x, y, data.health);
    this.loadTexture(data.plantAsset);
    this.patch = patch;

    this.animationName = null;
    // For private functions to work, you have to bind this.
    addAnimation.bind(this)(data);

    this.isShooter = data.isShooter;
    this.isSunProducer = data.isSunProducer;
    // If the plant is a shooter, we have to set up the timer
    if (this.isShooter) {
      this.shootingTimer.start();
      this.scheduleShooting();
    }

    if (this.isSunProducer) {
      this.producingTimer.start();
      this.scheduleProduction();
    }
  }
  kill() {
    super.kill();
    // Stop the timers!
    this.shootingTimer.stop();
    this.producingTimer.stop();
    // Also, free the patch
    this.patch.isBusy = false;
  }
  produceSun() {
    // Place the sun in a random location near the plant
    const diffX = -40 + (Math.random() * 80);
    const diffY = -40 + (Math.random() * 80);
    this.state.createSun(this.x + diffX, this.y + diffY);
  }
  scheduleProduction() {
    // Create a random sun
    this.produceSun();
    // Plants shoots once per second
    this.producingTimer.add(phaser.Timer.SECOND, this.scheduleProduction, this);
  }
  scheduleShooting() {
    this.shoot();
    // Plants shoots once per second
    this.shootingTimer.add(phaser.Timer.SECOND, this.scheduleShooting, this);
  }
  shoot() {
    if (this.animations.getAnimation(this.animationName)) {
      this.play(this.animationName);
    }
    // Location y of the bullet will be the mouth of the plant
    const y = this.position.y - 10;
    let bullet = this.bullets.getFirstDead();
    if (!bullet) {
      bullet = new Bullet(this.state, this.position.x, y);
      this.bullets.add(bullet);
    } else {
      bullet.reset(this.position.x, y);
    }
  }
}

