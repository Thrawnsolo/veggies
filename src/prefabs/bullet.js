import phaser from 'phaser';

export default class Bullet extends phaser.Sprite {
  constructor(state, x, y) {
    super(state.game, x, y, 'bullet');
    this.state = state;
    this.game = state.game;
    this.game.physics.arcade.enable(this);
    this.reset(x, y);
  }
  update() {
    if (this.x >= this.game.width) {
      this.kill();
    }
  }
  reset(x, y) {
    super.reset(x, y);
    this.body.velocity.x = 100;
  }
}
